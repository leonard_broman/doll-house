#include <Servo.h>

const int gateUpPin = 6;
const int gateDownPin = 7;
const int redLedPin =  3;
const int greenLedPin =  4;
const int servoPin = 9;

const int highPos = 160;
const int lowPos = 20;
const int gateSpeed = 50;
const int blinkTime = 500;

Servo gateServo;

int pos = lowPos;
int redMode = LOW;
int heartBeatState = 0;
unsigned long lastTime;
unsigned long redToggleTime;

#define MOVE_UP 1
#define MOVE_DOWN 2
#define DONT_MOVE 3
#define MOVE_INIT 4

int moving = MOVE_INIT;

void setup() {
  pinMode(gateUpPin, INPUT);
  pinMode(gateDownPin, INPUT);
  pinMode(redLedPin, OUTPUT);
  pinMode(greenLedPin, OUTPUT);
  pinMode(13, OUTPUT);
  digitalWrite(redLedPin, LOW);
  digitalWrite(greenLedPin, HIGH);
  lastTime = millis();
  redToggleTime = millis();
  gateServo.attach(9);
}

void loop() {
  unsigned long newTime = millis();
  int change = 0;

  if (digitalRead(gateUpPin) == HIGH) {
    moving = MOVE_UP;
    change = 1;
  } else if (digitalRead(gateDownPin) == HIGH) {
    moving = MOVE_DOWN;
    change = -1;
  } else {
    moving = DONT_MOVE;
    change = 0;
  }



  if (pos == highPos) {
    digitalWrite(greenLedPin, HIGH);
    redMode = LOW;
    digitalWrite(redLedPin, redMode);
  } else if (pos == lowPos) {
    digitalWrite(greenLedPin, LOW);
    redMode = HIGH;
    digitalWrite(redLedPin, redMode);
  } else if (moving == DONT_MOVE) {
    digitalWrite(greenLedPin, LOW);
    redMode = HIGH;
    digitalWrite(redLedPin, redMode);
  } else {
    digitalWrite(greenLedPin, LOW);
    if (redToggleTime + blinkTime < newTime) {
      redMode = (redMode == LOW ? HIGH : LOW);
      digitalWrite(redLedPin, redMode);
      redToggleTime = newTime;      
    }
  }

  
  
  if (newTime > lastTime + gateSpeed) {
    if (moving != DONT_MOVE) {
       digitalWrite(13,HIGH); 
    } else {
       digitalWrite(13,heartBeatState);      
    }
    heartBeatState = heartBeatState == HIGH ? LOW : HIGH;
    int newPos = pos + change;
    if (newPos != pos && newPos >= lowPos && newPos <= highPos) {
       pos = newPos;
       gateServo.write(newPos); 
    }
    lastTime = newTime;
  }
  
}
